const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers');

const{

	createUserController,
	getAllUsersController,
	getSingleUserController,
	updateSingleUserController

} = userControllers;

// create user
router.post('/', createUserController);
//get all users
router.get('/', getAllUsersController);
//get single user
router.get('/:id', getSingleUserController);
//update username
router.put('/:id', updateSingleUserController);

// export router which containts routes
module.exports = router;