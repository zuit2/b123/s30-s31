const express = require("express");
// Router() from express, allows access to HTTP method routes.
// router will act as middleware and our routing system
const router = express.Router();

// routes should only be concerned with endpoints and methods
// the action to be done once a route is accessed should be done in a separate file - our controllers.

const taskControllers = require('../controllers/taskControllers');

const {

	createTaskController,
	getAllTasksController,
	getSingleTaskController,
	completeTaskController,
	cancelTaskController

} = taskControllers;

router.post('/', createTaskController);

router.get('/', getAllTasksController);

router.get('/:id', getSingleTaskController);

router.put('/complete/:id', completeTaskController);

router.put('/cancel/:id', cancelTaskController);

module.exports = router;