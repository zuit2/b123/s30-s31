const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://toDoListUser:todolistuser-admin@sandboxdb.2ttig.gcp.mongodb.net/todoList123?retryWrites=true&w=majority"
	,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

//notifications if connection to db is success or failed:
let db = mongoose.connection;
db.on("error",console.error.bind(console, "connection error."));
db.once("open", ()=> console.log("Connected to MongoDB"));

// Middleware = middleware, in expressjs context, are methods, functions that act and add features to your application.
// handle json data from client
app.use(express.json());

const taskRoutes = require('./routes/taskRoutes');
app.use('/tasks',taskRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);


app.get('/hello',(req,res)=>{
	res.send('Hello from our new Express Api!');
})

app.listen(port, ()=>console.log(`Server is running at port ${port}`));