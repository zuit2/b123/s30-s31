const mongoose = require("mongoose");

// Mongoose Schema
	// Determine structure of documents. Acts as blueprint to how data/entries in collection should look like. Disallows to create docs that do not match the schema

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Mongoose model
	// Used to connect to your api

module.exports = mongoose.model("Task",taskSchema,"tasks");