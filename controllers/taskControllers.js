// import models
const Task = require('../models/Task');

module.exports.createTaskController = (req,res)=>{

	console.log(req.body);
	
	// Create a new Task document
	// check if there is a document with duplicate name field
	// Model.findOne() is a Mongoose method similar to findOne in MongoDB, however with Model.findOne(), we can process the result via our API.
	// .then() is able to capture the result of our query.
	// .then() is able to process the result and when that result is returned, we can add another then() to process the next result.
	// .catch() is able to capture the error of the query.
	Task.findOne({name: req.body.name})
	.then(result=>{

		console.log(result); //returns null if no documents were found with the name given in the criteria.
		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate Task Found");
		} else {
			// Create new task object out of our Task model
			let newTask = new Task({
			
				name: req.body.name

			});

			// .save() is a  method from an object 

			newTask.save()
			.then(result => res.send(result))
			.catch(err => res.send(err));		
		}
	})
	.catch(err => res.send(err));
};

module.exports.getAllTasksController = (req,res)=>{

	// Model.find() is a Mongoose method similar to MongoDB's find(). It is able to retrieve all documents that will match the criteria.
	Task.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

module.exports.getSingleTaskController = (req,res)=>{
	console.log(`-task id: ${req.params.id}`);
	Task.findById(req.params.id,{_id:0,name:1})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

module.exports.completeTaskController = (req,res) =>{
	console.log(req.params.id);
	let updates = {
		status: "complete"
	}
	Task.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedTask => res.send(updatedTask))
	.catch(err => res.send(err))
}

module.exports.cancelTaskController = (req,res) =>{
	console.log(req.params.id);
	let updates = {
		status: "cancelled"
	}
	Task.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedTask => res.send(updatedTask))
	.catch(err => res.send(err))
}


