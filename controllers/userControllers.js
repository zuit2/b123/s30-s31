const User = require('../models/User');

module.exports.createUserController = (req,res)=>{

	console.log(req.body);

	User.findOne({username:req.body.username},(err,result)=>{
		console.log(err);
		console.log(result);

		if (result !== null && result.username === req.body.username){
			res.send('Duplicate username found!');
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((saveErr,savedTask)=>{
				if(saveErr){
					console.log(saveErr);
				} else {
					res.send('Successful Registration!');
				}
			})
		}
	})

};

module.exports.getAllUsersController = (req,res)=>{

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));

};

module.exports.getSingleUserController = (req,res) => {
	// mongoose has a query called findById() which works like find {_id: "id"})
	console.log(req.params.id)//the id passed from your url.
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateSingleUserController = (req,res) => {

	console.log(req.params.id,);
	// Model.findByIdAndUpdate(id,{updates},{new:true})
	let updates = {
		username: req.body.username
	}
	User.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))
}